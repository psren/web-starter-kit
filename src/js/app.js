/*----------------------------------------------------------*\
 * jQuery
 *-----------------------------------------------------------
 *
 * Popular DOM-Animation and manipulation library.
 \*----------------------------------------------------------*/

/**
 * Bind jQuery to the window to allow other scripts
 * like Bootstrap to access the global jQuery object
 */
// window.$ = window.jQuery = require('jquery');


/*----------------------------------------------------------*\
 * VUEJS
 *-----------------------------------------------------------
 *
 * JS Library for building simple Pages with live-binding
 * or large scale Apps with packaged components.
\*----------------------------------------------------------*/

/**
 * Instantiate the VueJS main instance.
 * You can include other objects or partials as submodules
 * and extend your Application to your needs
 */

// import Vue from 'vue';

/**
 * You can include your own modules or vueify modules in here.
 * Think about registering them as components in the main VueJS instance
 */
//import Site from './Site.vue';

// new Vue({
//     el: 'body',
//     components: {
//         'site': Site
//     },
// });
