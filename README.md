
# Webstart

A small web starterkit for HTML/JS/CSS Projects which enforces strong standards.

## Used Software

- PostCSS
- Babel
- BassCSS
- Browser-Sync
- Browserify
- CSSMin
- CSSNext
- JSHint
- Stylelint
- UglifyJS
- Vueify
- VueJS

## Installation

Install all dependencies using npm
```
npm i
```

## Usage

There are several tasks defined. Look them up in package.json
The most important tasks are:

### Building for production

```
npm run build
```

### Develop

```
npm run develop
```

Or in silent mode (not as many ugly errors)

```
npm run develop -s
```

## Coming soon(ish)

- Yeoman generator (?)
- Static site generator / Template engine for quick mocking (?)
- Styledocs (?)
- Image optimizations
- Cache Buster (?)
