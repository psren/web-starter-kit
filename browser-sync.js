var bs = require("browser-sync").create();

// initialize a server variable we can set to false if there is a proxy defined
var server = true;

// get the proxy from the npm package configuration
var proxy = process.env.npm_package_config_host;

// if there is no proxy
if (proxy.length == 0) {
    var chalk = require("chalk");

    console.error(chalk.red.bold('If you use a vhost you need to set the host config like this to enable proxying: npm config set web-starter-kit:host <value>'));
    console.info(chalk.cyan('For example: npm set web-starter-kit:host webstarterkit.dev'));

    // set proxy to false to the browser-sync server is working as expected
    proxy = false;
} else {
    server = false;
}

bs.init({
    logLevel: 'info',
    open: false,
    server: server,
    proxy: proxy,
    files: ["*.html", "dist/css/**.css"]
});